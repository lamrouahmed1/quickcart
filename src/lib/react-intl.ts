import enMessages from '@translations/en';
import { flattenMessages } from '@utils/flattenMessages';

export enum LOCALES {
  EN = 'en',
  FR = 'fr',
}

const messages = {
  [LOCALES.EN]: flattenMessages(enMessages),
};

export const englishMessages = messages[LOCALES.EN];

export const defaultLocal = LOCALES.EN;
