import { flattenMessages } from "./flattenMessages";

test("flattenMessages Fn test", () => {
  const flattenedMessages = flattenMessages({
    key1: "value1",
    nestedKey: {
      key2: "value2",
    },
  });
  expect(flattenedMessages).toEqual({
    "key1": "value1",
    "nestedKey.key2": "value2",
  });
});
