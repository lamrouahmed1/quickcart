import styled from 'styled-components';

export const Container = styled.div`
  height: 30rem;
  width: 10rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  background-color: #fff;
  margin-bottom: 1rem;
  box-shadow: rgb(236, 231, 231) 0px 0px 5px 1px;
  transition: transform 200ms ease-in-out, box-shadow 200ms ease-in-out;
  cursor: pointer;

  &:hover {
    transition: transform 200ms ease-in-out, box-shadow 200ms ease-in-out;
    box-shadow: rgb(225, 221, 221) 0px 0px 5px 4px;
    transform: scale(0.99);
  }
`;

export const CardButton = styled.button`
  color: #fff;
  background-color: #000;
  width: 100%;
  border: unset;
  height: 3rem;
  font-size: 1.1rem;
  cursor: pointer;
`;

export const CardImage = styled.div`
  height: 18rem;
  width: 20rem;
  display: flex;
  align-items: center;
  justify-content: center;
  & > img {
    max-width: 10rem;
    height: auto;
  }
`;
export const Title = styled.span`
  text-align: center;
  padding: 0 1rem;
`;
export const Price = styled.span`
  font-size: 1.5rem;
`;
export const Rating = styled.div``;
