import { IProduct, currenciesEnum } from "@/shared/types";
import { addToCart } from "@/store/cart";
import { useAppDispatch } from "@/store/store";
import { MouseEvent } from "react";
import { FormattedMessage } from "react-intl";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CardImage,
  Title,
  Price,
  CardButton,
} from "./ProductCard.style";

const ProductCard = ({ id, image, price, title }: IProduct) => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const handleProductClick = (e: MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation();
    dispatch(addToCart({ id, quantity: 1 }));
  };
  return (
    <Container onClick={() => navigate(`/product/${id}`)}>
      <CardImage>
        <img src={image} />
      </CardImage>
      <Title>{title}</Title>
      <Price>
        <FormattedMessage
          id="shared.price"
          values={{ price, currency: currenciesEnum.dollar }}
        />
      </Price>
      <CardButton onClick={handleProductClick}>
        <FormattedMessage id="product.add-to-cart" />
      </CardButton>
    </Container>
  );
};

export default ProductCard;
