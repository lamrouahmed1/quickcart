import { productsSelector, setProducts } from '@/store/products';
import { useAppDispatch } from '@/store/store';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import ProductCard from './components/ProductCard';
import { ProductsWrapper } from './Home.style';
import { useProducts } from './api';
import Loader from '@components/Loader';

const Home = () => {
  const dispatch = useAppDispatch();
  const products = useSelector(productsSelector);

  const { data, isLoading } = useProducts({
    config: { cacheTime: 3600000, staleTime: 3600000 },
  });

  useEffect(() => {
    if (data) dispatch(setProducts(data));
  }, [data, dispatch]);

  if (isLoading) return <Loader />;
  if (!products.length) return null;
  return (
    <ProductsWrapper>
      {products.map((product) => (
        <ProductCard {...product} key={product.id} />
      ))}
    </ProductsWrapper>
  );
};

export default Home;
