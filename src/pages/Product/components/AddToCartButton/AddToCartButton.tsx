import { useAppDispatch } from '@/store/store';
import { FormattedMessage } from 'react-intl';
import { ButtonWrapper } from './AddToCartButton.style';
import { addToCart } from '@store/cart';
interface IProps {
  productId: number;
}

const AddToCartButton = ({ productId }: IProps) => {
  const dispatch = useAppDispatch();
  const handleClick = () => {
    dispatch(addToCart({ id: productId, quantity: 1 }));
  };
  return (
    <ButtonWrapper onClick={handleClick}>
      <FormattedMessage id="product.add-to-cart" />
    </ButtonWrapper>
  );
};

export default AddToCartButton;
