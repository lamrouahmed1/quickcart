import styled from 'styled-components';

export const ButtonWrapper = styled.button`
  background-color: #000;
  color: #fff;
  border: unset;
  height: 3rem;
  width: 10rem;
  cursor: pointer;
  border: 2px solid #000;
  transition: background-color 100ms ease-in-out, color 100ms ease-in-out;

  &:hover {
    background-color: #fff;
    color: #000;
    transition: background-color 100ms ease-in-out, color 100ms ease-in-out;
  }
`;
