import { ReactComponent as Star } from "@assets/star.svg";
import { Container } from "./Rating.style";

interface IRatingProps {
  rating: number;
}

const Rating = ({ rating }: IRatingProps) => {
  return (
    <Container>
      {[...Array(Math.floor(rating)).keys()].map(() => (
        <Star />
      ))}
    </Container>
  );
};

export default Rating;
