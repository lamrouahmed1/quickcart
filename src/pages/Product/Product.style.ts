import styled from "styled-components";

export const ProductContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  column-count: 2;
  height: calc(100% - 100px);

  & > div {
    width: 50%;
  }
`;

export const ProductImageContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #fff;
  height: 100%;

  & > img {
    max-width: 20rem;
    height: auto;
  }
`;

export const ProductDetailsContainer = styled.div`
  padding: 0 5rem;
`;

export const ProductName = styled.div`
  font-size: 2rem;
`;
export const ProductPrice = styled.div`
  font-size: 1.5rem;
  margin: 0.5rem 0;
`;
export const ProductDescription = styled.div`
  margin: 2rem 0;
  text-align: justify;
`;

export const ProductCategory = styled.div`
  font-weight: 400;
  margin-top: 1rem;
`;

export const RatingContainer = styled.div`
  margin-top: 2rem;
  display: flex;
  align-items: center;
  gap: 0.5rem;
  font-size: 1.2rem;
  font-weight: bold;
`;
