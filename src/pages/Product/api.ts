import { ExtractFnReturnType, QueryConfig } from '@/lib/react-query';
import { useQuery } from '@tanstack/react-query';
import { IProduct } from '@shared/types';

async function fetchProducts(productId?: string) {
  const response = await fetch(
    `https://fakestoreapi.com/products/${productId}`
  );
  return response.json();
}

export const getProduct = ({
  productId,
}: {
  productId?: string;
}): Promise<IProduct> => {
  return fetchProducts(productId);
};

type QueryFnType = typeof getProduct;

type UseProductOptions = {
  productId?: string;
  config?: QueryConfig<QueryFnType>;
};

export const useProduct = ({ productId, config }: UseProductOptions) => {
  return useQuery<ExtractFnReturnType<QueryFnType>>({
    ...config,
    queryKey: ['product', productId],
    queryFn: () => getProduct({ productId }),
    enabled: !!productId,
  });
};
