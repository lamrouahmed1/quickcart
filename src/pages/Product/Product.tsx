import { useParams } from "react-router-dom";
import AddToCartButton from "./components/AddToCartButton";
import Loader from "@components/Loader";
import Tag from "@components/Tag";
import { useProduct } from "./api";
import {
  ProductContainer,
  ProductDetailsContainer,
  ProductImageContainer,
  ProductName,
  ProductPrice,
  ProductDescription,
  ProductCategory,
  RatingContainer,
} from "./Product.style";
import { currenciesEnum } from "@/shared/types";
import { FormattedMessage } from "react-intl";
import Rating from "./components/Rating";

const Product = () => {
  const { productId } = useParams();
  const { data, isLoading } = useProduct({ productId });
  if (isLoading) return <Loader />;
  if (!data) return null;
  return (
    <ProductContainer>
      <ProductImageContainer>
        <img src={data.image} />
      </ProductImageContainer>
      <ProductDetailsContainer>
        <ProductName>{data.title}</ProductName>
        <ProductPrice>
          <FormattedMessage
            id="shared.price"
            values={{ price: data.price, currency: currenciesEnum.dollar }}
          />
        </ProductPrice>
        <ProductCategory>
          <Tag category={data.category} />
        </ProductCategory>
        <RatingContainer>
          <Rating rating={data.rating.rate} />({data.rating.count})
        </RatingContainer>
        <ProductDescription>{data.description}</ProductDescription>
        <AddToCartButton productId={data.id} />
      </ProductDetailsContainer>
    </ProductContainer>
  );
};

export default Product;
