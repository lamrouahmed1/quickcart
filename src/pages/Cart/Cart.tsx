import CartItem from "./components/CartItem";
import { ItemsContainer, TotalAmount } from "./Cart.style";
import { useCart } from "./hooks";
import { FormattedMessage } from "react-intl";
import { currenciesEnum } from "@/shared/types";
import EmptyCart from "./components/EmptyCart";

const Cart = () => {
  const { cart, total, isCartEmpty } = useCart();
  if (isCartEmpty) return <EmptyCart />;
  return (
    <>
      <ItemsContainer>
        {cart.products.map((product) => (
          <CartItem {...product} key={product.id} />
        ))}
      </ItemsContainer>
      {total > 0 && (
        <TotalAmount>
          <span>
            <FormattedMessage id="cart.total" />:
          </span>
          <FormattedMessage
            id="shared.price"
            values={{
              price: total.toFixed(2),
              currency: currenciesEnum.dollar,
            }}
          />
        </TotalAmount>
      )}
    </>
  );
};

export default Cart;
