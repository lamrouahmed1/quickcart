import { IProduct } from "@/shared/types";
import { cartSelector } from "@/store/cart";
import { productsSelector } from "@/store/products";
import { useAppSelector } from "@/store/store";

export const useCart = () => {
  const products = useAppSelector(productsSelector);
  const cart = useAppSelector(cartSelector);

  const total = cart.products.reduce((sum, currentProduct) => {
    const product = products.find(
      (p) => p.id === currentProduct.id
    ) as IProduct;
    return sum + product.price * currentProduct.quantity;
  }, 0);

  const isCartEmpty = cart.products.length === 0;

  return { total, cart, isCartEmpty };
};
