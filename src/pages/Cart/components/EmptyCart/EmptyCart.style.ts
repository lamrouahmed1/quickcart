import styled from "styled-components";

export const Container = styled.div`
  display: grid;
  place-content: center;
  font-weight: bold;
  font-size: 2rem;
  height: calc(100% - 100px);
`;
