import { FormattedMessage } from "react-intl";
import { Container } from "./EmptyCart.style";

const EmptyCart = () => {
  return (
    <Container>
      <FormattedMessage id="cart.empty-cart" />
    </Container>
  );
};

export default EmptyCart;
