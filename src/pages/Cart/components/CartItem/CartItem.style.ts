import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  margin-bottom: 1rem;
  background-color: #fff;
  padding: 1rem 0;
`;

export const ImageContainer = styled.div`
  & > img {
    width: 3rem;
    height: auto;
  }
`;

export const InfoContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Title = styled.span`
  font-weight: 600;
  flex-basis: 40%;
`;
export const Description = styled.span`
  font-weight: 400;
`;
export const Price = styled.span`
  font-weight: 600;
`;
export const CategoryContainer = styled.div`
  display: flex;
  justify-content: center;
  flex-basis: 30%;
`;

export const CartActionsContainer = styled.div`
`;

export const RightSectionContainer = styled.div`
  display: flex;
  align-items: center;
  flex-basis: 40%;
  gap: 2rem;
`;
