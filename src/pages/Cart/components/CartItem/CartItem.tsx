import { ICartProduct, IProduct, currenciesEnum } from "@/shared/types";
import {
  Container,
  ImageContainer,
  CategoryContainer,
  Title,
  Price,
  RightSectionContainer,
  CartActionsContainer,
} from "./CartItem.style";
import { useAppSelector } from "@/store/store";
import { productsSelector } from "@/store/products";
import CartActions from "../CartActions";
import Tag from "@/components/Tag";
import { FormattedMessage } from "react-intl";

const CartItem = ({ id, quantity }: ICartProduct) => {
  const products = useAppSelector(productsSelector);
  const { title, category, image, price } =
    products.find((p) => p.id === id) || ({} as IProduct);
  return (
    <Container>
      <ImageContainer>
        <img src={image} />
      </ImageContainer>
      <Title>{title}</Title>
      <RightSectionContainer>
        <CategoryContainer>
          <Tag category={category} />
        </CategoryContainer>
        <CartActionsContainer>
          <CartActions quantity={quantity} productId={id} />
        </CartActionsContainer>
        <Price>
          <FormattedMessage
            id="shared.price"
            values={{
              price: (price * quantity).toFixed(2),
              currency: currenciesEnum.dollar,
            }}
          />
        </Price>
      </RightSectionContainer>
    </Container>
  );
};

export default CartItem;
