import { decreaseItem, increaseItem, removeFromCart } from "@/store/cart";
import { useAppDispatch } from "@/store/store";

export const useCartActions = (productId: number, quantity: number) => {
  const dispatch = useAppDispatch();
  const decreaseQte = () => {
    if (quantity > 1) dispatch(decreaseItem(productId));
  };
  const increaseQte = () => {
    dispatch(increaseItem(productId));
  };
  
  const removeItem = () => {
    dispatch(removeFromCart(productId));
  };
  return { decreaseQte, increaseQte, removeItem };
};
