import {
  Container,
  PlusButton,
  Quantity,
  MinusButton,
  RemoveButton,
} from "./CartActions.style";
import { memo } from "react";
import { useCartActions } from "./hooks";

interface IProps {
  quantity: number;
  productId: number;
}

const CartActions = memo(({ quantity, productId }: IProps) => {
  const { decreaseQte, increaseQte, removeItem } = useCartActions(productId, quantity);
  return (
    <Container>
      <PlusButton onClick={increaseQte}>+</PlusButton>
      <Quantity>{quantity}</Quantity>
      <MinusButton onClick={decreaseQte} disabled={quantity === 1}>
        -
      </MinusButton>
      <RemoveButton onClick={removeItem}>x</RemoveButton>
    </Container>
  );
});

export default CartActions;
