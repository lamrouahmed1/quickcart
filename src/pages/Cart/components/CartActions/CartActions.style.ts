import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

interface IBtnProps {
  disabled?: boolean;
}

const CartButton = styled.button<IBtnProps>`
  border: 1px solid #000;
  height: 2rem;
  width: 2rem;
  background-color: #fff;
  position: relative;
  cursor: ${({ disabled }) => disabled ? 'not-allowed' : 'pointer'};
  &:hover {
    opacity: .8;
  }
`;

export const Quantity = styled(CartButton)`
  border: unset;
`;

export const PlusButton = styled(CartButton)`
  color: #fff;
  background-color: #000;
`;

export const MinusButton = styled(CartButton)``;
export const RemoveButton = styled(CartButton)`
  margin-left: 2rem;
  font-weight: 600;
  color: #fff;
  background-color: #f46464;
  border: unset;
`;
