import styled from "styled-components";

export const ItemsContainer = styled.div`
  padding: 5rem;
  margin: 0 auto;
`;

export const TotalAmount = styled.div`
  position: sticky;
  bottom: 0;
  top: 100%;
  background-color: #fff;
  height: 5rem;
  font-size: 2rem;
  display: flex;
  align-items: center;
  justify-content: end;
  padding: 0 2rem;
  gap: 2rem;
`;
