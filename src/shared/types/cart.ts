export interface ICartProduct {
  id: number;
  quantity: number;
}

export interface ICart {
  products: ICartProduct[];
}
