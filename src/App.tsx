import { AppProvider } from '@providers/app';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Header from './components/Header';
import Product from '@pages/Product';
import Home from '@pages/Home';
import Cart from '@pages/Cart';

const App = () => {
  return (
    <AppProvider>
      <BrowserRouter>
        <Header />
        <Routes>
          <Route element={<Home />} path={'/'} />
          <Route element={<Cart />} path={'/cart'} />
          <Route element={<Product />} path={'/product/:productId'} />
        </Routes>
      </BrowserRouter>
    </AppProvider>
  );
};

export default App;
