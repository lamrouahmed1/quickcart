import { render, screen } from "@testing-library/react";
import Tag from "../Tag";

test("Tag component should render the tag passed in props", () => {
  render(<Tag category="test-category" />);
  expect(screen.queryByText("test-category")).toBeInTheDocument();
});
