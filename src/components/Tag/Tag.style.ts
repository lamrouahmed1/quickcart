import styled from 'styled-components';

export const TagContainer = styled.span`
  font-size: 0.8rem;
  background-color: #000;
  color: #FFF;
  padding: 0.3rem 1rem;
  border-radius: 1rem;
  display: inline-block;
  text-align: center;
`;
