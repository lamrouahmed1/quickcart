import { TagContainer } from './Tag.style';

interface ITagProps {
  category: string;
}

const Tag = ({ category }: ITagProps) => {
  return (
    <TagContainer>{category}</TagContainer>
  );
};

export default Tag;
