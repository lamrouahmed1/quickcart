import { Triangle } from 'react-loader-spinner';
import { LoaderContainer } from './Loader.style';

const Loader = () => {
  return (
    <LoaderContainer>
      <Triangle color="#000" />
    </LoaderContainer>
  );
};

export default Loader;
