import { render } from "@testing-library/react";
import Loader from "../Loader";

test("Loader component renders correctly", () => {
  const { container } = render(<Loader />);
  expect(container).toMatchSnapshot();
});
