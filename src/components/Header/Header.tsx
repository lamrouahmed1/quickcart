import { CartContainer, HeaderWrapper, LogoContainer } from "./Header.style";
import { ReactComponent as StoreIcon } from "@assets/logo.svg";
import { ReactComponent as CartIcon } from "@assets/cart.svg";
import { cartSelector } from "@/store/cart";
import { useAppSelector } from "@/store/store";
import { useNavigate } from "react-router-dom";

const Header = () => {
  const cart = useAppSelector(cartSelector);
  const navigate = useNavigate();

  return (
    <HeaderWrapper>
      <LogoContainer onClick={() => navigate("/")} className="to-home">
        <StoreIcon />
      </LogoContainer>
      <CartContainer
        isCartPopulated={!!cart.products.length}
        onClick={() => navigate("/cart")}
        className="to-cart"
      >
        <CartIcon />
      </CartContainer>
    </HeaderWrapper>
  );
};

export default Header;
