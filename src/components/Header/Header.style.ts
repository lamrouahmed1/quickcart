import styled from 'styled-components';

export const HeaderWrapper = styled.header`
  background-color: #fff;
  display: flex;
  height: 100px;
  align-items: center;
  justify-content: space-between;
`;

export const HeaderItem = styled.div`
  cursor: pointer;
  display: flex;
  height: 100%;
  align-items: center;
  justify-content: center;
  width: 6rem;
  border-bottom: 3px solid transparent;
  transition: border 200ms ease-in-out;

  &:hover {
    transition: border 200ms ease-in-out;
    border-bottom: 3px solid #22331d;
  }
`;

export const LogoContainer = styled(HeaderItem)``;

interface ICartProps {
  isCartPopulated: boolean;
}

export const CartContainer = styled(HeaderItem)<ICartProps>`
  position: relative;
  &::after {
    content: '';
    position: absolute;
    background-color: ${(props) =>
      props.isCartPopulated ? 'red' : 'transparent'};
    width: 5px;
    height: 5px;
    border-radius: 100%;
    top: 1rem;
    right: 1.5rem;
    transition: background-color: 400ms ease-in-out;
  }
`;
