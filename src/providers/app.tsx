import * as React from 'react';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl';
import { queryClient } from '@/lib/react-query';
import { defaultLocal, englishMessages } from '@/lib/react-intl';
import store from '@/store';
import { QueryClientProvider } from '@tanstack/react-query';

type AppProviderProps = {
  children: React.ReactNode;
};

export const AppProvider = ({ children }: AppProviderProps) => {
  return (
    <Provider store={store}>
      <QueryClientProvider client={queryClient}>
        <IntlProvider locale={defaultLocal} messages={englishMessages}>
          {children}
        </IntlProvider>
      </QueryClientProvider>
    </Provider>
  );
};
