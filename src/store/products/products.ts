import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { PRODUCTS } from '../constants';
import { RootState } from '../reducer';
import { IProduct } from '@shared/types';

type TProducts = IProduct[];

const initialState: TProducts = [];

const productSlice = createSlice({
  name: PRODUCTS,
  initialState,
  reducers: {
    setProducts: (_, action: PayloadAction<IProduct[]>) => action.payload,
  },
});

export const productsSelector = (state: RootState) => state.products;

export const { setProducts } = productSlice.actions;

export default productSlice.reducer;
