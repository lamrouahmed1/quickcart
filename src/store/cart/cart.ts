import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { CART } from "../constants";
import { RootState } from "../reducer";
import { ICart, ICartProduct } from "@/shared/types";

const initialState: ICart = {
  products: [],
};

const cartSlice = createSlice({
  name: CART,
  initialState,
  reducers: {
    addToCart: (state, action: PayloadAction<ICartProduct>) => {
      const oldProduct = state.products.find(
        (product) => product.id === action.payload.id
      );
      if (oldProduct) {
        state.products.forEach((product) => {
          if (product.id === oldProduct.id)
            oldProduct.quantity += product.quantity;
        });
      } else {
        state.products.push(action.payload);
      }
    },
    removeFromCart: (state, action: PayloadAction<number>) => {
      state.products = state.products.filter(
        (product) => product.id !== action.payload
      );
    },
    increaseItem: (state, action: PayloadAction<number>) => {
      state.products = state.products.map((p) => {
        if (p.id === action.payload) p.quantity++;
        return p;
      });
    },
    decreaseItem: (state, action: PayloadAction<number>) => {
      state.products = state.products.map((p) => {
        if (p.id === action.payload) p.quantity--;
        return p;
      });
    },
    clearCart: (state) => {
      state.products = [];
    },
  },
});

export const cartSelector = (state: RootState) => state.cart;

export const {
  addToCart,
  removeFromCart,
  decreaseItem,
  increaseItem,
  clearCart,
} = cartSlice.actions;

export default cartSlice.reducer;
