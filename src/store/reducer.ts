import { combineReducers } from '@reduxjs/toolkit';
import cartReducer from './cart';
import productsReducer from './products';
import { CART, PRODUCTS } from './constants';

export const rootReducer = combineReducers({
  [CART]: cartReducer,
  [PRODUCTS]: productsReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
