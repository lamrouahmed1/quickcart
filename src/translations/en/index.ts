import cart from './cart.json';
import product from './product.json';
import shared from './shared.json';

export default {
  cart: { ...cart },
  product: { ...product },
  shared: { ...shared },
};
