
# QuickCart App

Welcome to the QuickCart App! This is a sample React application for an online shopping cart.

## Demo

You can access the live demo of the QuickCart App at [https://quickcartproject.netlify.app/](https://quickcartproject.netlify.app/).
  
## Getting Started

**To get started with the QuickCart App on your local machine, follow these steps:**

 1. Clone the repository: `git clone https://gitlab.com/lamrouahmed1/quickcart.git`
 2. Navigate to the project directory: `cd quickcart`
 3. install the dependencies: `npm i`
4. start the development server: `npm run dev`

The app will be available at http://localhost:3000


## Running Unit Tests

To run the unit tests for the QuickCart App, use the following command: `npm run test`. This command will execute the unit tests and display the test results.

If you want to generate a coverage report, you can run: `npm run test:coverage`.

The coverage report will be available in the coverage directory.
  
## Running End-to-End Tests

**The QuickCart App includes end-to-end tests using Cypress. To run the end-to-end tests, follow these steps:**
  
1. Start the development server:  `npm run dev`

2. Open the Cypress Test Runner GUI: `npm run cypress:open`. This will launch the Cypress Test Runner GUI.

3. In the Cypress Test Runner, click on a test file to run its associated tests. You can also run all the tests by clicking the **"Run all specs"** button.

Note: Make sure the application is running at **http://localhost:3000** before running the Cypress tests.

