describe("User Flow - Cart", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000");
  });

  it("cart should be empty if no product was added", () => {
    //click the cart icon
    cy.get(".to-cart").click();

    //check if the cart is empty
    cy.contains(
      "Your cart is empty. Start shopping now to add items to your cart."
    ).should("be.visible");
  });

  it("adds an product to the cart and removes it", () => {

    //click the home button
    cy.get(".to-home").click();
    
    //add the first product to the cart by clicking on 'Add to cart'
    cy.contains("Add to cart").should("be.visible").click();

    //go the cart to see if the product was added 
    cy.get(".to-cart").click();

    //check if the cart is not empty
    cy.contains(
      "Your cart is empty. Start shopping now to add items to your cart."
    ).should("not.exist");

    //check if the total amount appears on the screen
    cy.contains("Total").should("be.visible");
    
    //click on 'x' to remove the product from the cart
    cy.contains("x").click();

    //check if the cart is empty
    cy.contains(
      "Your cart is empty. Start shopping now to add items to your cart."
    ).should("be.visible");
  });
});
